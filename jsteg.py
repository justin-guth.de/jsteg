from PIL import Image


def prepare(image: Image.Image, bits: int):

    for x in range(image.width):
        for y in range(image.height):

            pixel = image.getpixel((x, y))

            newPixel = (
                (pixel[0] >> bits) << bits,
                (pixel[1] >> bits) << bits,
                (pixel[2] >> bits) << bits
            )

            image.putpixel((x, y), newPixel)

def reduce(image: Image.Image, bits: int):

    for x in range(image.width):
        for y in range(image.height):

            pixel = image.getpixel((x, y))

            newPixel = (
                int((pixel[0] / 256) * (1 << bits)),
                int((pixel[1] / 256) * (1 << bits)),
                int((pixel[2] / 256) * (1 << bits))
            )

            image.putpixel((x, y), newPixel)

def expand(image: Image.Image, bits: int):

    for x in range(image.width):
        for y in range(image.height):

            pixel = image.getpixel((x, y))

            newPixel = (
                int((pixel[0] / (1 << bits)) * 256),
                int((pixel[1] / (1 << bits)) * 256),
                int((pixel[2] / (1 << bits)) * 256)
            )

            image.putpixel((x, y), newPixel)

def hide(surrogate: Image.Image, message: Image.Image, bits: int):

    if surrogate.width != message.width or surrogate.height != message.height:

        raise Exception("Message and surrogate dimensions must be equal.")

    surrogate = surrogate.copy()
    message = message.copy()

    prepare(surrogate, bits)
    reduce(message, bits)

    for x in range(surrogate.width):
        for y in range(surrogate.height):

            surrogatePixel = surrogate.getpixel((x, y))
            messagePixel = message.getpixel((x, y))

            newPixel = (
                int(surrogatePixel[0] | messagePixel[0]),
                int(surrogatePixel[1] | messagePixel[1]),
                int(surrogatePixel[2] | messagePixel[2])
            )

            surrogate.putpixel((x, y), newPixel)

    return surrogate

def extract(surrogate: Image.Image, bits: int):
    
    result = surrogate.copy()

    for x in range(surrogate.width):
        for y in range(surrogate.height):

            surrogatePixel = surrogate.getpixel((x, y))

            newPixel = (
                int((surrogatePixel[0] & ((1 << bits) - 1)) * 256 / (1 << bits)),
                int((surrogatePixel[1] & ((1 << bits) - 1)) * 256 / (1 << bits)),
                int((surrogatePixel[2] & ((1 << bits) - 1)) * 256 / (1 << bits))
            )

            result.putpixel((x, y), newPixel)

    return result

if __name__ == "__main__":

    bits = 3

    euler = Image.open("./euler.jpg")
    cat = Image.open("./catn.jpg")

    hidden = hide(euler, cat, bits)
    image = extract(hidden, bits)

    hidden.show()
    image.show()

    hidden.save("./hidden.png", "PNG")
    image.save("./message.png", "PNG")
